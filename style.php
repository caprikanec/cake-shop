<?php
	require_once "variable.php";
	echo ("
	<style>
		@font-face{
			font-family: 'arial narrow';
			src: url('font/ARIALNBI.TTF');
			font-weight: bold;
			font-style: italic;
		}
		*{ 
		  -moz-user-select: none; 
		  -o-user-select:none; 
		  -khtml-user-select: none; 
		  -webkit-user-select: none; 
		  -ms-user-select: none; 
		  user-select: none; 
		}
		body, div, span, h1, h2, h3, h4, h5, h6, p, em, img, strong, sub, sup, b, u, i,  dl, dt, dd, ol, ul, li, fieldset, form, label, table, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, figcaption, figure, footer, header, hgroup, menu, nav, section, summary, time, mark, audio, video {
			margin: 0;
			padding: 0;
			border: 0;
			outline: 0;                               
			vertical-align: baseline;          
			background: transparent; 
			font-size: 100%;      
		}
		html {
			margin: 0;
			padding: 0;
			border: 0;
			outline: 0;                               
			vertical-align: baseline;          
			background-image: url(USERFILES/bg.jpg);
			background-size: ; 
			font-size: 100%;      
		}
		a {                       
			margin:0;
			padding:0;
			font-size:100%;
			vertical-align:baseline;
			background:transparent;
			outline:none;
		}
		table {						
			border-collapse: collapse; 
			border-spacing: 0;
		}
		td, td img {
			vertical-align: top;		
		} 
		input, select, button, textarea {
			padding: 0;
			margin: 0; 				
			font-size: 100%; 
			outline: none;
		}
		input[type=text], input[type=password], input[type=submit], textarea {
			padding: 0;
		}
		input[type=checkbox] {
			vertical-align: bottom;
		}
		input[type=radio] {
			vertical-align: text-bottom;
		}
		article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {
			display:block;
		}
		nav ul {
			 list-style:none;
		}
		#break{
			width: $x98".px.";
			height: $x2".px.";
			margin: 0 auto;
		}
		#main{
			width:$wb".px.";
			height:auto;
			margin:0 auto;
		}
		
		
									/*-------Шапка-----*/
	.body {
		width: $x100".px.";
		height: auto;
		background: $white;
		margin: 0 auto;
	}
	.head {
		width: $x100".px.";
		height: $x16".px.";
		background: $light_blue;
		position: relative;
		margin: 0 auto;
	}
	.siteName {
		width: $x68".px.";
		height: $x6".px.";
		position: absolute;
		left: $x16".px.";
		top: 0;
		font:$anbi;
		font-size: $x5".px.";
		color: $blue;
		line-height: $x6".px.";
		text-align: center;
	}
	.description {
		width: $x68".px.";
		height: $x5".px.";
		position: absolute;
		left: $x16".px.";
		top: $x6".px.";
		font:$anbi;
		font-size: $x3_5".px.";
		color: $blue;
		line-height: $x5".px.";
		text-align: center;
	}
	.phone_text {
		width: $x28".px.";
		height: $x5".px.";
		position: absolute;
		left: 0;
		top: $x11".px.";
		font:$anbi;
		font-size: $x3_5".px.";
		background: $blue;
		color: $white;
		line-height: $x5".px.";
		text-align: left;
	}
	.location {
		width: $x44".px.";
		height: $x5".px.";
		position: absolute;
		left: $x28".px.";
		top: $x11".px.";
		font:$anbi;
		font-size: $x3_5".px.";
		background: $blue;
		color: $light_blue;
		line-height: $x5".px.";
		text-align: center;
	}
	.message_text {
		width: $x28".px.";
		height: $x5".px.";
		position: absolute;
		left: $x72".px.";
		top: $x11".px.";
		font:$anbi;
		font-size: $x3_5".px.";
		background: $blue;
		color: $white;
		line-height: $x5".px.";
		text-align: right;
	}
	.phone_button {
		width: $x16".px.";
		height:$x11".px.";
		position: absolute;
		left: 0;
		top: 0;
	}
	.message_button {
		width: $x16".px.";
		height: $x11".px.";
		position: absolute;
		left: $x84".px.";
		top: 0;
	}
	.phone_text_button {
		width: $x28".px.";
		height: $x7".px.";
		position: absolute;
		left: 0;
		top: $x11".px.";
	}
	.location_button {
		width: $x44".px.";
		height: $x7".px.";
		position: absolute;
		left: $x28".px.";
		top: $x11".px.";
	}
	.message_text_button {
		width: $x28".px.";
		height: $x7".px.";
		position: absolute;
		left: $x72".px.";
		top: $x11".px.";
	}
		
		
		
		#head1, #head2, #info1, #info2, #info3, #info4, #info5, #option1, #option2, #option3, #option4, #menu1, #menu2, #menu2 div, #menu3, #menu3 div, #menu4, #menu4 form{
			width: $x98".px.";
			height: auto;
			margin: 0 auto;
			text-align:center;
		}
		#head1, #head2 .first_div, #info1 .first_div, #info2 .first_div, #info3 .first_div, #info4, #info5{
			position:relative;
		}
		#head1>p, #head2 .first_div p, #info1 .first_div p, #info2 .first_div p, #info3 .first_div p, #info4>p, #info5>p, #option1 .second div p, #option2 .second div p, #option3 .second div p, #option4 .second div p{
			font:$anbi;
			font-size:$large".px.";
			color:$blue;
			line-height:$x6".px.";
		}
		#head1 img, #head2 div img, #info1 div img, #info2 div img, #info3 div img, #info4 img, #info5 img{
			width:$x3".px.";
			height:$x3".px.";
		}
		#head1 div, #head2 div, #info1 div, #info2 div, #info3 div, #info4 div, #info5 div{
			width:$x98".px.";
			height:auto;
		}
		#head1>div>p, #head2 div p, #info1 div p, #info2 div p, #info3 div p, #info4 div p, #info5 div p, #option1 .first p, #option2 .first p, #option3 .first p, #option4 .first p, #menu2 div[name=menu2_more] div p, #menu3 div[name=menu3_more] div p, #menu4 form a, #menu4 form div p{
			font:$anbi;
			font-size:$medium".px.";
			color:$blue;
			line-height:$x4".px.";
		}
		#head1 div[name=head1_more], #head2 div[name=head2_more], #info1 div[name=info1_more], #info2 div[name=info2_more], #info3 div[name=info3_more], #info4 div[name=info4_more], #info5 div[name=info5_more], #menu2 div[name=menu2_more], #menu3 div[name=menu3_more], #menu4 form{
			display:none;
		}
		#head2 img, #info1 img, #info2 img, #info3 img, #menu2 div[name=menu2_more] div:last-child img, #menu3 div[name=menu3_more] div:last-child img{
			width:$x11".px.";
			height:$x11".px.";
			margin:0 auto;
		}
		#option1, #option2, #option3, #option4{
			width:$x98".px.";
			height:$x16".px.";
			margin:0 auto;
			position:relative;
		}
		#option1 .first, #option2 .first, #option3 .first, #option4 .first{
			width:$x98".px.";
			height:$x5".px.";
			position:absolute;
			top:0;
			left:0;
		}
		#option1 .first p, #option2 .first p, #option3 .first p, #option4 .first p{
			line-height:$x5".px.";
		}
		#option1 .second, #option2 .second, #option3 .second, #option4 .second{
			width:$x96".px.";
			height:$x9".px.";
			background-color:rgb(240,254,240);
			border:$x1".px." solid $light_blue;
			border-radius:$x5_5".px.";
			position:absolute;
			top:$x5".px.";
			left:0;
		}
		#option1 .second div, #option2 .second div, #option3 .second div, #option4 .second div{
			width:$x76".px.";
			height:$x9".px.";
			position:absolute;
			top:$x0".px.";
			left:$x1".px.";
		}
		#option1 .second div p, #option2 .second div p, #option3 .second div p, #option4 .second div p{
			line-height:$x9".px.";
		}
		#option1 .threeth, #option2 .threeth, #option3 .threeth, #option4 .threeth{
			width:$x19".px.";
			height:$x9".px.";
			background-color:$blue;
			border:$x1".px." solid $light_blue;
			border-radius:$x5_5".px.";
			position:absolute;
			top:$x5".px.";
			left:$x77".px.";
		}
		#option1 .threeth img, #option2 .threeth img, #option3 .threeth img, #option4 .threeth img{
			width:$x19".px.";
			height:$x9".px.";
		}
		.opt_button{
			width: $x98".px.";
			height: $x11".px.";
			position: absolute;
			top: $x5".px.";
			left: 0;
		}
		#menu1{
			width:$x98".px.";
			height:$x8".px.";
			background-color:$light_blue;
			margin:0 auto;
		}
		#menu1>p, #menu2 div:first-child p, #menu3 div:first-child p, #menu4 div:first-child p, #menu4 form input[name=name], #menu4 form input[name=phone], #menu4 form textarea, #menu4 form input[name=send]{
			font:$anbi;
			font-size:$large".px.";
			color:$blue;
			line-height:$x8".px.";
		}
		#menu2 .first, #menu3 .first, #menu4 .first{
			width:$x98".px.";
			height:$x8".px.";
			background-color:$light_blue;
			margin:0 auto;
		}
		#menu2 div[name=info2_more] div:last-child, #menu3 div[name=info3_more] div:last-child{
			height:$x11".px.";
		}
		#menu4{
			height:$x54".px.";
		}
		#menu4 form{
			height:$x46".px.";
			position:relative;
		}
		#menu4 form input, #menu4 form textarea{
			text-align:center;
		}
		#menu4 form input[name=name], #menu4 form input[name=phone], #menu4 form textarea, #menu4 form input[name=send]{
			width:$x47".px.";
			height:$x9".px.";
			background-color:$white;
			border:$x1".px." solid $light_blue;
			border-radius:$x5_5".px.";
			position:absolute;
			top:$x2".px.";
			left:0;
		}
		#menu4 form input[name=name], #menu4 form input[name=phone], #menu4 form input[name=send]{
			line-height:$x9".px.";
		}
		#menu4 form input[name=phone]{
			left:$x49".px.";
		}
		#menu4 form textarea{
  			resize: none;
  			overflow: hidden;
			width:$x96".px.";
			line-height:$x9".px.";
			border:$x1".px." solid $blue;
			top:$x14".px.";
		} 
		#menu4 form img{
			width:$x8".px.";
			height:$x8".px.";
			position:absolute;
			top:$x25".px.";
			left:0;
		}
		#menu4 form div{
			position:absolute;
			width:$x90".px.";
			height:$x8".px.";
			top:$x25".px.";
			left:$x8".px.";
		}
		#menu4 form a, #menu4 form div p{
			color:$light_blue;
			line-height:$x4".px.";
		}
		#menu4 form input[name=send]{
			width:$x98".px.";
			height:$x11".px.";
			background-color:$blue;
			top:$x33".px.";
			color:$white;
		}
		
		
		 /*------Настройка всплывающих infoblock------*/
		 
		 .wrap{
		 	width:$width".px.";
		 	height:$height".px.";
			background-color:gray;
			opacity:0.4;
		 	position:fixed;
		 	left:0;
			z-index:2;
		 	display:none;
		 }
		 .wrapShow{
		 	display:block;
		 }
		.infoblock { 
			width: $x100".px.";
			height: $x20".px.";
			background: $blue;
			display:none;
			position: fixed;
			top: 0;
			padding: 0;
			margin: 0 auto;
			z-index:2;
		}
		.limiter { 
			width: $x100".px.";
			height: $x1".px.";
			background: $light_blue;
			position: absolute;
			top:$x19".px.";
			margin: 0 auto;
		}
		.about { 
			width: $x100".px.";
			height: $x4".px.";
			position: absolute;
			top: 0;
			font: $anbi;
			font-size: $x3_5".px.";
			color: $white;
			line-height: $x4".px.";
			text-align: center;
		}
		.form {
			width: $x72".px.";
			height: $x9".px.";
			background: $white;
			border-style: solid;
			border-radius: $x5_5".px.";
			border-width: $x1".px.";
			border-color: $light_blue;
			position: absolute;
			top: $x5_5".px.";
			left: $x1".px.";
			font: $anbi;
			font-size: $x5".px.";
			color: $blue;
			line-height: $x9".px.";
			text-align: center;
		}
		.enter {
			width: $x22".px.";
			height: $x9".px.";
			background: $light_blue;
			border-style: solid;
			border-radius: $x5_5".px.";
			border-width: $x1".px.";
			border-color: $white;
			position: absolute;
			top: $x5_5".px.";
			left: $x75".px.";
			background-image: url(USERFILES/enter.png);
			background-size: contain;
			font: $anbi;
			font-size: $x5".px.";
			color: $blue;
			line-height: $x9".px.";
			text-align: left;
		}
		.enter_button {
			width: $x25".px.";
			height: $x14_5".px.";
			position: absolute;
			top: $x5_5".px.";
			left: $x75".px.";
		}
		.exit_button {
			width: $x10".px.";
			height: $x5_5".px.";
			position: absolute;
			top: 0;
			left: $x90".px.";
		}
		
						
	.b_f, .button_more {
		width: $x96".px.";
		height: $x9".px.";
		font:$anbi;
		font-size: $x5".px.";
		color: rgb(254,254,254);
		line-height: $x9".px.";
		text-align: center;
		background: rgb(0,141,210);
		border-style: solid;
		border-radius: $x5_5".px.";
		border-width: $x1".px.";
		border-color: rgb(162,217,247);
		margin: 0 auto;
		z-index:20;
	}
	.show_button, .show_more_button{
		width: $x98".px.";
		height: $x11".px.";
		position:absolute;
		top:0;
		left:$x1".px.";
		z-index:21;
	}
	.loadTotalCount{
		width: $x6_5".px.";
		height: $x9".px.";
		position: absolute;
		top: $x1".px.";
		left: $x42".px.";
		background-color: $blue;
	}
	.buttonLoad{
		position: relative;
	}
	</style>
	");
?>