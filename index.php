﻿<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Тортыназаказ.москва</title>
		<meta name="viewport" content="width=600px, user-scalable=no">
		<script src="js\jquery-3.2.0.min.js"></script>
		<script src="js\jquery-ui.min.js"></script>
		<script src="js\maskPhon.js"></script>
	</head>
	<body>
		<div id="main">
			<div id="main_block"></div>
			<div id="rezult_block"></div>
			<div id="menu_block"></div>
		</div>
		<script src="js\ajaxScript.js" type="text/javascript"></script>
		<script src="js\functionScript.js" type="text/javascript"></script>
	</body>
</html>