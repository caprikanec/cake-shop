﻿	var timeShow = 150;
	function openClose_head1(){
		$("div[name=head1_more]").slideToggle(timeShow);
	}
	function openClose_head2(){
		$("div[name=head2_more]").slideToggle(timeShow);
	}
	function open_menu4(){
		$("#menu4").show(timeShow);
		var menu4Scroll = $("#menu4").offset().top;
		$(window).scrollTop(menu4Scroll);
	}
	function openClose_info1(){
		$("div[name=info1_more]").slideToggle(timeShow);
	}
	function moveInfo1(){
		var moveInfo1 = $('#info1').offset().top;
		$(window).scrollTop(moveInfo1);
	}
	function openClose_info2(){
		$("div[name=info2_more]").slideToggle(timeShow);
	}
	function moveInfo2(){
		var moveInfo2 = $('#info2').offset().top;
		$(window).scrollTop(moveInfo2);
	}
	function openClose_info3(){
		$("div[name=info3_more]").slideToggle(timeShow);
	}
	function moveInfo3(){
		var moveInfo3 = $('#info3').offset().top;
		$(window).scrollTop(moveInfo3);
	}
	function openClose_info4(){
		$("div[name=info4_more]").slideToggle(timeShow);
	}
	function openClose_info5(){
		$("div[name=info5_more]").slideToggle(timeShow);
	}
	function moveHead1(){
		var moveHead1 = $('.head').offset().top;
		$(window).scrollTop(moveHead1);
	}
	function openClose_menu2(){
		$("div[name=menu2_more]").slideToggle(timeShow);
	}
	function openClose_menu3(){
		$("div[name=menu3_more]").slideToggle(timeShow);
	}
	function openClose_menu4(){
		$("#menu4 form").slideToggle(timeShow);
		var moveMenu4 = $('#menu4').offset().top;
		setTimeout(function(){$(window).scrollTop(moveMenu4)}, timeShow);
	}

	$(document).on("click", '.buttonMainBlock', function(){
		$(this).parent().next().next().slideUp(timeShow);
		$(this).parent().next().slideToggle(timeShow);
	});
	
	$(document).on("click", '.telForIfone', function(){
		$(this).prev().find('.call_img').click();
	});
	
	$(document).on("click", ".telForIfoneDesc", function(){
		$(this).parent().next().slideToggle(timeShow);
	});
	
	$(document).on('click', '.message_button, .message_text_button', function(){
		$("#menu4 form").slideDown(timeShow);
		var moveMenu4 = $('#menu4').offset().top;
		setTimeout(function(){$(window).scrollTop(moveMenu4)}, timeShow);
	});

	$(document).on('click', '.phone_text_button', function(){
		$('.phone_button').click();
	})


				/*------Работа переключателей option-------*/

	$(document).on("click", '.opt_button', function(){
        if(!/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)){
        	$('.rezult').removeClass('block').addClass('none');
			$('.button_more').css({'display':'none'});
			$('.show_more_button').css({'display':'none'});
			$('.formMore').css({'display':'none'});
			$('.rezult_more').css({'display':'none'});
			$('.result_form_more').slideUp();
			titleKrestoff();
			var th = $(this).parent();
			if (th.hasClass('active')){
				th.removeClass('active').children('.threeth').children().attr({"src":"USERFILES/option_off.png"});
				th.children('.threeth').css({"backgroundColor":$('.siteName').css('color')});
			}else{
				th.addClass('active').children('.threeth').css({"backgroundColor":$('#colorPos').css('color')});
				th.children('.threeth').children().attr({"src":"USERFILES/option_on.png"});
			}
			countActiveResult(th.attr('id'));
			countResOnButton();
        }
	});

	function move(ev){
            $('.rezult').removeClass('block').addClass('none');
            $('.button_more').css({'display': 'none'});
            $('.formMore').css({'display': 'none'});
            $('.rezult_more').css({'display': 'none'});
            titleKrestoff();
            var th = $('#option' + ev);
            if (th.hasClass('active')) {
                th.removeClass('active').children('.threeth').children().attr({"src": "USERFILES/option_off.png"});
                th.children('.threeth').css({"backgroundColor": $('.siteName').css('color')});
            } else {
                th.addClass('active').children('.threeth').css({"backgroundColor": $('#colorPos').css('color')});
                th.children('.threeth').children().attr({"src": "USERFILES/option_on.png"});
            }
            countActiveResult($('#option' + ev + ' img').parent().parent().attr('id'));
            countResOnButton();
 }


			//------Добавление блоков REZULT-------//
	
$(document).on('click', '.show_button, .show_more_button', function(){
	var addBlocks = 5;
	$(this).css({'display':'none'});
	$('.button_more').css({'display':'none'});
	$('.b_f').css({'display':'none'});
	for (var i=0; i<addBlocks; i++){
		$('.showMe').eq(0).addClass('block').removeClass('none').removeClass('showMe');
	}
	if ($('.showMe').length > 0){
		$('.button_more').css({'display':'block'});
		$('.show_more_button').css({'display':'block'});
	}
});


			//-----Checkbox------//

$(document).on('click', '.checkboxButton', function(){
	$(this).parent().find('.checkbox').click();
});
$(document).on('click', '.checkbox', function(){
		var data = $(this).attr('data');
		if (data==1){
			$(this).attr({"data":"0"}).css({"backgroundColor":$('#colorNeg').css('color')});
		}else{
			$(this).attr({"data":"1"}).css({"backgroundColor":$('#main').css('backgroundColor')});
		}			
	});
	
	
			/*-----Блок текст/фото-----*/

$(document).on('click', '.title_krest_off', function(){
    $(this).parent().find('.option').css({'display':'block'});
    $(this).parent().find('.imgAndText').css({'display':'none'});
    $(this).parent().find('.active').removeClass('active').css({'borderColor':$('#option1 .second').css('borderColor'),
																'backgroundImage':'url(USERFILES/eye.png)'});
});

var check = 1;
$(document).on('click', '.title_select_for_ifone', function(){
	var th = $(this).parent().parent().parent();
	var src = serchSrc($(this).prev()).split('.')[0];
	if (check){
		th.find('.s_baze').css({'display':'none'});
		th.find('.baze').css({'backgroundImage':'url('+src+'.jpg)'}).next().children('.cameraButton').css({'opacity':0});
		check = 0;
		return false;
	}
	if (!check){
		th.find('.s_baze').css({'display':'table-cell'});
		th.find('.baze').css({'backgroundImage':'none'}).next().children('.cameraButton').css({'opacity':1});
		check = 1;
	}
});


			//--------Работа блоков проигрывания---------//

var numberOfTrack;
var step = 0;
var trackTitle;
var	maxPos;
var	maxNeg;
$(document).on("click", ".prevButton, .nextButton, .playButton", function(){
	if($(this).hasClass('prevButton'))
		$(this).parent().find('#previous').click();
	if($(this).hasClass('nextButton'))
		$(this).parent().find('#next').click();
	if($(this).hasClass('playButton'))
		$(this).parent().find('#play').click();
});
$(document).on("click", "#previous, .prevBig, #next, .nextBig, #play", function(){
	var type = $(this).data('type');
	var diraction = $(this).attr('id');
	var par = $(this).parent().parent().parent();
	if ($(this).hasClass('prevBig'))
		diraction = 'previous';
	if ($(this).hasClass('nextBig'))
		diraction = 'next';
	trackTitle = par.find('div[data='+type+']').children("#colorPos, #colorNeg").text();
	numberOfTrack = parseInt(trackTitle.split('-')[0]);
	maxPos = par.find('p[id=muzGreen]').text();
	maxNeg = par.find('p[id=muzRed]').text();
	
	if(numberOfTrack == 0) return false;
	if (!par.find('#negInfoBlock, #posInfoBlock').hasClass('active')){
		par.find('#posInfoBlock').addClass('active').css({'borderColor':$('.siteName').css('color'),
														'backgroundImage':'url(USERFILES/cross.png)'});
	}		
	
	if (diraction === "previous"){step = -1;}
	if (diraction === "next"){step = 1;}
	if (diraction === "previous" && numberOfTrack > 1){
		remoteNumberTrackWithOutPlay($(this));
        fitback($(this));
		if (par.find('#play').hasClass('plaing')){
			par.find('div[data='+type+']').children('audio').trigger('play');
		}
		if (par.find('.baze').css('backgroundImage') != 'none'){
			var src = serchSrc($(this)).split('.')[0];
			par.find('.baze').css({'backgroundImage':'url('+src+'.jpg)'});
		}
	}
	if (diraction === "next" && numberOfTrack < parseInt(par.find('div[data='+type+']').children("#muzGreen, #muzRed").text())){
		remoteNumberTrackWithOutPlay($(this));
        fitback($(this));
		if (par.find('#play').hasClass('plaing')){
			par.find('div[data='+type+']').children('audio').trigger('play');
		}
		if (par.find('.baze').css('backgroundImage') != 'none'){
			src = serchSrc($(this)).split('.')[0];
			par.find('.baze').css({'backgroundImage':'url('+src+'.jpg)'});
		}
	}
	
	if (diraction === "play"){
		fitback($(this));
		if ($(this).hasClass('plaing')){
			$(this).attr({"src":"USERFILES/play.png"}).removeClass('plaing');
			par.find('div[data='+type+']').children('audio').trigger('pause');
		}else{
			$(this).addClass('plaing').attr({"src":"USERFILES/pause.png"});
			par.find('div[data='+type+']').children('audio').trigger('play');
		}
	}
	par.find('.option').css({'display':'none'});
	par.find('.imgAndText').css({'display':'block'});

    var img = new Image();
    par.find('.cameraButton').css({'display':'none'})
    img.src = serchSrc($(this)).split('.')[0] + '.jpg';
    img.onload = function(){par.find('.cameraButton').css({'display':'block'})};
});

function fitback(th){
		var src = serchSrc(th).split('.')[0];
		src = src.split('/');
		var numFitback = parseInt(src[src.length-1]);
		var fitb = "";
		numFitback --;
		if (th.data('type') === 'pos'){
			fitb = th.parent().parent().parent().parent().find('.fitbackGood').eq(numFitback).text();
		th.parent().parent().parent().find('.s_baze').text(fitb);
		}
		if (th.data('type') === 'neg'){
			fitb = th.parent().parent().parent().parent().find('.fitbackBed').eq(numFitback).text();
		th.parent().parent().parent().find('.s_baze').text(fitb);
		}
		if (th.attr('data') === 'pos'){
			fitb = th.parent().parent().find('.fitbackGood').eq(numFitback).text();
		th.parent().find('.s_baze').text(fitb);
		}
		if (th.attr('data') === 'neg'){
			fitb = th.parent().parent().find('.fitbackBed').eq(numFitback).text();
		th.parent().find('.s_baze').text(fitb);
		}
}


			//---------Переключение между отзывами----------//

$(document).on('click', '.negInfo, .posInfo', function(){
	var par = $(this).parent();
    var img = new Image();
    par.find('.cameraButton').css({'display':'none'})
    img.src = serchSrc($(this)).split('.')[0] + '.jpg';
    img.onload = function(){par.find('.cameraButton').css({'display':'block'})};

	var th;
	if ($(this).hasClass('posInfo')){
		th = $(this).parent().find('#posInfoBlock')
	}else if ($(this).hasClass('negInfo')){
		th = $(this).parent().find('#negInfoBlock')
	}
	th.parent().find('audio').trigger('pause');
	th.parent().find('.playDiv').children().attr({"src":"USERFILES/play.png"}).removeClass('plaing');
		th.parent().find('.option').css({'display':'none'});
		th.parent().find('.imgAndText').css({'display':'block'});
		if (th.hasClass('active')){
			th.parent().find('.title_krest_off').click();
			return false;
		}
		if (th.attr('id') === 'posInfoBlock' && !th.hasClass('active')){
			th.next().removeClass('active').css({'borderColor':th.css('borderColor'),
														'backgroundImage':'url(USERFILES/eye.png)'});
			th.addClass('active').css({'borderColor':$('.siteName').css('color'),
														'backgroundImage':'url(USERFILES/cross.png)'});
			th.parent().find('.negPlayer').css({'display':'none'});
			th.parent().find('.posPlayer').css({'display':'block'});
			th.parent().find('.prevBig, .nextBig, .cameraButton').data('type', 'pos').attr('data-type', 'pos');
			th.parent().find('.title_krest_off').removeClass("title_krest_off_neg");
			fitback(th);
		}else if (th.attr('id') === 'negInfoBlock' && !th.hasClass('active')){
			th.prev().removeClass('active').css({'borderColor':th.css('borderColor'),
														'backgroundImage':'url(USERFILES/eye.png)'});
			th.addClass('active').css({'borderColor':$('.siteName').css('color'),
											'backgroundImage':'url(USERFILES/cross.png)'});
			th.parent().find('.posPlayer').css({'display':'none'});
			th.parent().find('.negPlayer').css({'display':'block'});
			th.parent().find('.prevBig, .nextBig, .cameraButton').data('type', 'neg').attr('data-type', 'neg');
			th.parent().find('.title_krest_off').addClass("title_krest_off_neg");
			fitback(th);
		}
		if (th.hasClass('active')){
			bigButtonReaction(th);
		}
});

function bigButtonReaction(th){
	var type = th.attr('data');
	var active2Prev = th.parent().find('#previous[data-type='+type+']').data('active2');
	var active2Next = th.parent().find('#next[data-type='+type+']').data('active2');
	
	th.parent().find('.prevBig').data('active2', active2Prev).attr('data-active2', active2Prev);
	th.parent().find('.nextBig').data('active2', active2Next).attr('data-active2', active2Next);
	if (active2Prev){
		th.parent().find('.prevBig').css({'display':'block'});
	}else if (active2Prev === 0){
		th.parent().find('.prevBig').css({'display':'none'});
	}
	if (active2Next){
		th.parent().find('.nextBig').css({'display':'block'});
	}else if (active2Next === 0	){
		th.parent().find('.nextBig').css({'display':'none'});
	}
}


			//---------Заполнение форм----------//

$(document).on('click', '.nameForm', function(){
	$(this).parent().find('.name').click();
});
$(document).on('click', '.phoneForm', function(){
	$(this).parent().find('.phone').click();
});

$(document).on('click', '.name, .phone', function(){
	$('input[name=phone]').css({"borderColor":$('#head1>p').css('color')});
	$('.wrap').addClass('wrapShow');
    $('.infoblock').attr({'data':$(this).parent().parent().attr('id')});
	
	if ($(this).hasClass('name')){
		$('#infoblock1').slideToggle(timeShow);
		$('#infoblock1 .form').focus();
	}
	if ($(this).hasClass('phone')){
		$('#infoblock2').slideToggle(timeShow);
		$('#infoblock2 .form').focus();
	}
});

$(document).on('click', '.specificForm, .fotoForm, .promoForm', function(){
	if ($(this).hasClass('specificForm'))
		$(this).parent().find('#specific').click();
	if ($(this).hasClass('fotoForm'))
		$(this).parent().find('.loadImg').click();
	if ($(this).hasClass('promoForm'))
		$(this).parent().find('#promo').click();
});

$(document).on('click', '#specific, .loadImg, #promo', function(){
    $('.infoblock').attr({'data':$(this).parent().parent().parent().parent().attr('id')});
	if ($(this).attr('id') == 'specific'){
        $('.wrap').addClass('wrapShow');
		$('#infoblock3').slideToggle(timeShow);
		$('#infoblock3 .form').focus();
	}
	if ($(this).hasClass('loadImg')){
		$(this).parent().next().children().click();
	}
	if ($(this).attr('id') == 'promo'){
        $('.wrap').addClass('wrapShow');
		$('#infoblock4').slideToggle(timeShow);
		$('#infoblock4 .form').focus();
	}
});

$(document).on('click', '.wrap, .exit_button', function(){
	$('.wrap').removeClass('wrapShow');
	$('.infoblock').slideUp(timeShow);
});

var placeholder = '';
$(document).on('focus', 'input, textarea', function(){
	if ($(this).attr('type') != 'submit'){
		$(this).removeClass('phcolor');
		$(this).addClass('phcolorGray');
		placeholder = $(this).attr('placeholder');
	}
	if ($(this).attr('name') == 'phone')
		$(this).mask("+7 (999) 999-9999");
});

var name2 = '';
var phone = '';
var specific = '';
var photo = '';
var promo = '';
var text = '';
$(document).on('focusout', 'input, textarea', function(){
	$(this).removeClass('phcolorGray');
	$(this).addClass('phcolor');
	var name = $(this).attr('name');
	var val = $(this).val();
	$("input[name="+name+"]").val(val);
	$('.'+name).text(val);
	if (val == ''){
		$('.'+name).text('Ваше имя');
       // $('.name').attr('data', 0);
		$(this).attr({'placeholder':placeholder});
	}//else{
		//$('.name').attr('data', 1);
	//}
	if ($('.phone').text().indexOf('_') > 0){
		$('.phone').text('Ваш телефон');
	}
	$('input[name=phone]').each(function(){
		if ($(this).val().indexOf('_') > 0){
			$(this).val('Ваш телефон');
		}
	});
	if ($(this).attr('name') === 'name')
		name2 = $(this).val();
	if ($(this).attr('name') === 'phone')
		phone = $(this).val();
	if ($(this).attr('name') === 'specific')
		specific = $(this).val();
	if ($(this).attr('name') === 'photo')
		photo = $(this).val();
	if ($(this).attr('name') === 'promo')
		promo = $(this).val();
	if ($(this).attr('name') === 'text')
		text = $(this).val();
	
	$.ajax({
		url: 'form.php',
		type: 'get',
		data: {'name':name2, 'phone':phone, 'specific':specific, 'photo':photo, 'promo':promo, 'text':text}
	})
});


			//-------Обработка нажания на Enter---------//

	function keyEvent(block){
        $('#infoblock' + block).focusout();
		if (block === 'sendMess')
			$('#sandMenu4').click();
		else
			$('#infoblock' + block).find('.enter_button').click();
	}


			//-------Отправка письма---------//
		
$(document).on('click', '.sendForm', function(){
    $('.infoblock').attr({'data':$(this).parent().parent().attr('id')});
	$(this).parent().find('.send').click();
});
$(document).on('click', '.send, #sandMenu4, .enter_button, .photoUrlForm, .promoUrlForm', function(){
	if ($(this).hasClass('enter_button'))
		var id = $(this).parent().attr('data');
	else
		var id = $(this).parent().parent().attr('id');
	var th = $(this);
	var nameForm = id+'_form_more';
	var flag = '';
	if ($(this).attr('id')=='sandMenu4')
		flag = 'sendMenu4';
	if (($(this).parent().find('img.checkbox').attr('data') == 1 && flag == '') || ($(this).parent().parent().parent().find('img.checkbox').attr('data') == 1 && flag == '') || $(this).hasClass('enter_button')){
		if ($('#infoblock2 .phone').val() !== ''){
			$('.infoblock').slideUp(timeShow);
            $('.wrap').removeClass('wrapShow');
			var form = document.forms.namedItem(nameForm);
            var formData = new FormData(form);
            formData.append('id',id);
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "mail.php");
            xhr.send(formData);
            
        	$.ajax({
				url: 'mail.php',
				type: 'post',
				data: {'send_down':'send'},
				success: function(data,s){
                    $('#'+id).find('.send').text(data);
					if (th.hasClass('send')){
						$('#'+id).find('.send').css({'borderColor':$('#colorPos').css('color')});
					}else{
						th.text(data).css({'borderColor':$('#colorPos').css('color')});
					}
                    $('#'+id).find('.phone').css({'borderColor':$('#colorPos').css('color')});
					if ($('input[name=name]').val() != '')
                        $('#'+id).find('.name').css({'borderColor':$('#colorPos').css('color')});
					if (th.hasClass('photoUrlForm'))
                        $('#'+id).find('#photo_url').css({'borderColor':$('#colorPos').css('color')});
					if (th.hasClass('promoUrlForm'))
                        $('#'+id).find('#promo_url').css({'borderColor':$('#colorPos').css('color')});

                    $('#'+id).find('input').each(function(){
                    	if ($(this).val() !== '') {
                            $(this).css({'borderColor': $('#colorPos').css('color')});
                        }
					});
				}
			});
				return false;
		}else{
			$('#infoblock1, #infoblock3, #infoblock4').slideUp(timeShow);
			$('.wrap').addClass('wrapShow');
			$('#infoblock2').slideDown(timeShow);
			$('#infoblock2 .phone').focus();
			return false;
		};
	}else if (flag == ''){
		return false;
	}
	
	if ($(this).parent().find('img').attr('data') == 1 && flag == 'sendMenu4'){
		if ($(this).parent().find('textarea').val() !== ''){
            $(this).parent().find('textarea').css({"borderColor":$('.siteName').css('color')});
            if ($('#phoneMenu4').val() !== '') {
                $('#phoneMenu4').css({"borderColor":$('.siteName').css('color')});
                $.ajax({
                    url: 'mail.php',
                    type: 'post',
                    data: {'id': id, 'flag': flag},
                    success: function (data, s) {
                        th.parent().find('textarea').css({"borderColor": $('.head_text1').css('color')});
                        th.parent().find('input[name=send]').val('Отправлено');
                    }
                });
                return false;
            }else {
            	$('#phoneMenu4').css({"borderColor":$('#colorNeg').css('color')});
                return false;
            }
		}else{
			$(this).parent().find('textarea').css({"borderColor":$('#colorNeg').css('color')});
			return false;
		};
	}else{
		return false;
	}
	return false;
});


			/*---------Уберём всё лишнее из адресной строки----------*/

 window.onhashchange = function () {
        history.pushState('', '',  ' ');
};


				/*--------Запуск загрузки фото-----------*/

$(document).on('change', 'input[type=file]', function(){
    if ($(this).val() != '') {
        $.ajax({
            url: 'mail.php',
            type: 'post',
            data: {'upload': 'load'},
            success: function (data, s) {
                $('.loadImg').val(data).attr('value', data);
            }
        });
        return false;
    }
	return false;
});


				/*-----------Пользовательское соглашение------------*/
				
$(document).on('click', '#head .description a, .rezult div:nth-child(2) #IAgreeMyData a, #menu4 form div', function(){
		openClose_menu2();
		var href = $(this).parent().parent().parent().parent().attr('id');
		$('#menu2 a[id=back]').attr({'href':'#'+href});
});


				/*-----------Отзывы------------*/
				
$(document).on('click', '#info2 a', function(){
		openClose_menu3();
		var href = $(this).parent().parent().parent().attr('id');
		$('#menu3 a[id=back3]').attr({'href':'#'+href});
});


				/*-----------Рабочие функции-----------*/
				
function remoteNumberTrackWithOutPlay(th){
	var player;
	var soundSorce;
	var soundTrack;
	var numberTrack;
	var type = th.data('type');
	var src = '';
	numberOfTrack += step;
	numberOfTrack += "-" + trackTitle.split('-')[1];
	if (th.data('type') === 'neg'){
		th.parent().parent().parent().find("#colorNeg").text(numberOfTrack + '-');
		player = th.parent().parent().parent().find('#colorNeg').next();
	}else{
		th.parent().parent().parent().find("#colorPos").text(numberOfTrack);
		player = th.parent().parent().parent().find('#colorPos').next();
	}
	soundSorce = player.attr('src');
	soundTrack = soundSorce.split('/');
	numberTrack = parseInt(soundTrack[3].split('.')[0]);
	if (step === -1){
		th.parent().parent().parent().find("#next[data-type="+type+"], .nextBig").data('active2', 1).attr('data-active2', 1);
		th.parent().parent().parent().find("#next[data-type="+type+"]").attr({"src":"USERFILES/next_on.png"});
		th.parent().parent().parent().find(".nextBig").css({"display":"block"});
		src = soundTrack[0] + '/' + soundTrack[1] + '/' + soundTrack[2] + '/' + (numberTrack-1 + '.mp3');
		player.attr({"src":src});
	}else if (step === 1){
		th.parent().parent().parent().find("#previous[data-type="+type+"], .prevBig").data('active2', 1).attr('data-active2', 1);
		th.parent().parent().parent().find("#previous[data-type="+type+"]").attr({"src":"USERFILES/previous_on.png"});
		th.parent().parent().parent().find(".prevBig").css({"display":"block"});
		src = soundTrack[0] + '/' + soundTrack[1] + '/' + soundTrack[2] + '/' + (numberTrack+1 + '.mp3');
		player.attr({"src":src});
	}
	if (step == -1 && parseInt(trackTitle.split('-')[0]) == 2){
		th.parent().parent().parent().find("#previous[data-type="+type+"], .prevBig").data('active2', 0).attr('data-active2', 0);
		th.parent().parent().parent().find("#previous[data-type="+type+"]").attr({"src":"USERFILES/previous_off.png"});
		th.parent().parent().parent().find(".prevBig").css({"display":"none"});
	}
	if ((step == 1 && parseInt(trackTitle.split('-')[0]) == maxPos-1 && th.data('type')=='pos') || (step == 1 && parseInt(trackTitle.split('-')[0]) == maxNeg-1 && th.data('type')=='neg')){
		th.parent().parent().parent().find("#next[data-type="+type+"], .nextBig").data('active2', 0).attr('data-active2', 0);
		th.parent().parent().parent().find("#next[data-type="+type+"]").attr({"src":"USERFILES/next_off.png"});
		th.parent().parent().parent().find(".nextBig").css({"display":"none"});
	}
}

function serchSrc(th){
	if (th.data('type') === 'pos'){
		var src = th.parent().parent().parent().find('p[id=colorPos]').next().attr('src');
	}else if (th.data('type') === 'neg'){
		var src = th.parent().parent().parent().find('p[id=colorNeg]').next().attr('src');
	}
	if (th.attr('data') === 'pos'){
		var src = th.parent().find('p[id=colorPos]').next().attr('src');
	}else if (th.attr('data') === 'neg'){
		var src = th.parent().find('p[id=colorNeg]').next().attr('src');
	}
	return src;
}

function countActiveResult(id) {
	$('.rezult').each(function(){
		if (!$(this).hasClass('showMe') && !$(this).hasClass('hideMe')){
			$(this).addClass('showMe');
		}
	});
	if ($('#' + id).hasClass('active')) {
		$('.showMe').each(function () {
			if ($(this).data(id) === 0) {
				$(this).removeClass('showMe').addClass('hideMe');
			}
		})
	} else {
        $('.rezult').removeClass('hideMe').addClass('showMe');
        $('.option').each(function () {
            if ($(this).hasClass('active')) {
                var thisOption = $(this).attr('id');
                $('.showMe').each(function () {
                    if ($(this).data(thisOption) === 0) {
                        $(this).removeClass('showMe').addClass('hideMe');
                    }
                });
            }
        })
    }
}

function countResOnButton(){
	$('.b_f').addClass('button_first');
	$('.show_button').css({'display':'block'});
	$('.rezult').addClass('none');
	var countRez = $('.showMe').length;
    var santans = $('.button_first').text();
	var santans2 = "";
	santans = santans.split(" ");
	for (var i=0; i < santans.length; i++){
		if (i == 2 && santans[2] == 0){continue;}
		if (i==1){
			santans2 += countRez + " ";
			continue;
		}
		if (i < santans.length-1)
			santans2 += santans[i] + " ";
		else
			santans2 += santans[i];
	}
	$('.button_first').text(santans2).css({'display':'block'});
}

function titleKrestoff() {
	$('.option').css({'display':'block'});
	$('.imgAndText').css({'display':'none'});
};