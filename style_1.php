<?php
	require_once "variable.php";
	echo ("
	<style>
		@font-face{
			font-family: 'arial narrow';
			src: url('font/ARIALNBI.TTF');
			font-weight: bold;
			font-style: italic;
		}
		*{ 
		  -moz-user-select: none; 
		  -o-user-select:none; 
		  -khtml-user-select: none; 
		  -webkit-user-select: none; 
		  -ms-user-select: none; 
		  user-select: none; 
		}
		body, div, span, h1, h2, h3, h4, h5, h6, p, em, img, strong, sub, sup, b, u, i,  dl, dt, dd, ol, ul, li, fieldset, form, label, table, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, figcaption, figure, footer, header, hgroup, menu, nav, section, summary, time, mark, audio, video {
			margin: 0;
			padding: 0;
			border: 0;
			outline: 0;                               
			vertical-align: baseline;          
			background: transparent; 
			font-size: 100%;      
		}
		html {
			margin: 0;
			padding: 0;
			border: 0;
			outline: 0;                               
			vertical-align: baseline;          
			background-image: url(USERFILES/bg.jpg);
			background-size: ; 
			font-size: 100%;      
		}
		a {                       
			margin:0;
			padding:0;
			font-size:100%;
			vertical-align:baseline;
			background:transparent;
			outline:none;
		}
		table {						
			border-collapse: collapse; 
			border-spacing: 0;
		}
		td, td img {
			vertical-align: top;		
		} 
		input, select, button, textarea {
			padding: 0;
			margin: 0; 				
			font-size: 100%; 
			outline: none;
		}
		input[type=text], input[type=password], input[type=submit], textarea {
			padding: 0;
		}
		input[type=checkbox] {
			vertical-align: bottom;
		}
		input[type=radio] {
			vertical-align: text-bottom;
		}
		article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {
			display:block;
		}
		nav ul {
			 list-style:none;
		}
		#break{
			width: $x98".px.";
			height: $x2".px.";
			margin: 0 auto;
		}
		#main{
			width:$wb".px.";
			height:auto;
			margin:0 auto;
		}
		#head1, #head2, #info1, #info2, #info3, #info4, #info5, #option1, #option2, #option3, #option4, #menu1, #menu2, #menu2 div, #menu3, #menu3 div, #menu4, #menu4 form{
			width: $x98".px.";
			height: auto;
			margin: 0 auto;
			text-align:center;
		}
		#head1, #head2 .first_div, #info1 .first_div, #info2 .first_div, #info3 .first_div, #info4, #info5{
			position:relative;
		}
		#head1>p, #head2 .first_div p, #info1 .first_div p, #info2 .first_div p, #info3 .first_div p, #info4>p, #info5>p, #option1 .second div p, #option2 .second div p, #option3 .second div p, #option4 .second div p{
			font:$anbi;
			font-size:$large".px.";
			color:$blue;
			line-height:$x6".px.";
		}
		#head1 img, #head2 div img, #info1 div img, #info2 div img, #info3 div img, #info4 img, #info5 img{
			width:$x3".px.";
			height:$x3".px.";
		}
		#head1 div, #head2 div, #info1 div, #info2 div, #info3 div, #info4 div, #info5 div{
			width:$x98".px.";
			height:auto;
		}
		#head1>div>p, #head2 div p, #info1 div p, #info2 div p, #info3 div p, #info4 div p, #info5 div p, #option1 .first p, #option2 .first p, #option3 .first p, #option4 .first p, #menu2 div[name=menu2_more] div p, #menu3 div[name=menu3_more] div p, #menu4 form a, #menu4 form div p{
			font:$anbi;
			font-size:$medium".px.";
			color:$blue;
			line-height:$x4".px.";
		}
		#head1 div[name=head1_more], #head2 div[name=head2_more], #info1 div[name=info1_more], #info2 div[name=info2_more], #info3 div[name=info3_more], #info4 div[name=info4_more], #info5 div[name=info5_more], #menu2 div[name=menu2_more], #menu3 div[name=menu3_more], #menu4 form{
			display:none;
		}
		#head2 img, #info1 img, #info2 img, #info3 img, #menu2 div[name=menu2_more] div:last-child img, #menu3 div[name=menu3_more] div:last-child img{
			width:$x11".px.";
			height:$x11".px.";
			margin:0 auto;
		}
		#option1, #option2, #option3, #option4{
			width:$x98".px.";
			height:$x16".px.";
			margin:0 auto;
			position:relative;
		}
		#option1 .first, #option2 .first, #option3 .first, #option4 .first{
			width:$x98".px.";
			height:$x5".px.";
			position:absolute;
			top:0;
			left:0;
		}
		#option1 .first p, #option2 .first p, #option3 .first p, #option4 .first p{
			line-height:$x5".px.";
		}
		#option1 .second, #option2 .second, #option3 .second, #option4 .second{
			width:$x96".px.";
			height:$x9".px.";
			background-color:rgb(240,254,240);
			border:$x1".px." solid $light_blue;
			border-radius:$x5_5".px.";
			position:absolute;
			top:$x5".px.";
			left:0;
		}
		#option1 .second div, #option2 .second div, #option3 .second div, #option4 .second div{
			width:$x76".px.";
			height:$x9".px.";
			position:absolute;
			top:$x0".px.";
			left:$x1".px.";
		}
		#option1 .second div p, #option2 .second div p, #option3 .second div p, #option4 .second div p{
			line-height:$x9".px.";
		}
		#option1 .threeth, #option2 .threeth, #option3 .threeth, #option4 .threeth{
			width:$x19".px.";
			height:$x9".px.";
			background-color:$blue;
			border:$x1".px." solid $light_blue;
			border-radius:$x5_5".px.";
			position:absolute;
			top:$x5".px.";
			left:$x77".px.";
		}
		#option1 .threeth img, #option2 .threeth img, #option3 .threeth img, #option4 .threeth img{
			width:$x19".px.";
			height:$x9".px.";
		}
		#menu1{
			width:$x98".px.";
			height:$x8".px.";
			background-color:$light_blue;
			margin:0 auto;
		}
		#menu1>p, #menu2 div:first-child p, #menu3 div:first-child p, #menu4 div:first-child p, #menu4 form input[name=name], #menu4 form input[name=phone], #menu4 form textarea, #menu4 form input[name=send]{
			font:$anbi;
			font-size:$large".px.";
			color:$blue;
			line-height:$x8".px.";
		}
		#menu2 .first, #menu3 .first, #menu4 .first{
			width:$x98".px.";
			height:$x8".px.";
			background-color:$light_blue;
			margin:0 auto;
		}
		#menu2 div[name=info2_more] div:last-child, #menu3 div[name=info3_more] div:last-child{
			height:$x11".px.";
		}
		#menu4{
			height:$x54".px.";
		}
		#menu4 form{
			height:$x46".px.";
			position:relative;
		}
		#menu4 form input, #menu4 form textarea{
			text-align:center;
		}
		#menu4 form input[name=name], #menu4 form input[name=phone], #menu4 form textarea, #menu4 form input[name=send]{
			width:$x47".px.";
			height:$x9".px.";
			background-color:$white;
			border:$x1".px." solid $light_blue;
			border-radius:$x5_5".px.";
			position:absolute;
			top:$x2".px.";
			left:0;
		}
		#menu4 form input[name=name], #menu4 form input[name=phone], #menu4 form input[name=send]{
			line-height:$x9".px.";
		}
		#menu4 form input[name=phone]{
			left:$x49".px.";
		}
		#menu4 form textarea{
  			resize: none;
  			overflow: hidden;
			width:$x96".px.";
			line-height:$x9".px.";
			border:$x1".px." solid $blue;
			top:$x14".px.";
		} 
		#menu4 form img{
			width:$x8".px.";
			height:$x8".px.";
			position:absolute;
			top:$x25".px.";
			left:0;
		}
		#menu4 form div{
			position:absolute;
			width:$x90".px.";
			height:$x8".px.";
			top:$x25".px.";
			left:$x8".px.";
		}
		#menu4 form a, #menu4 form div p{
			color:$light_blue;
			line-height:$x4".px.";
		}
		#menu4 form input[name=send]{
			width:$x98".px.";
			height:$x11".px.";
			background-color:$blue;
			top:$x33".px.";
			color:$white;
		}
		
		
		 /*------Настройка всплывающих infoblock------*/
		 
		 .wrap{
		 	width:$width".px.";
		 	height:$height".px.";
		 	position:fixed;
		 	left:0;
			z-index:2;
		 	display:none;
		 }
		 .wrapShow{
		 	display:block;
		 }
		.infoblock {
			display:none;
			width: $wb".px.";
			height: $x20".px.";
			background: rgb(240,254,240);
			position: fixed;
			top: 0;
			z-index:2;
		}
		.infoblock_limiter { 
			width: $wb".px.";
			height: $x1".px.";
			background: rgb(0,141,210);
			position: absolute;
			top: $x19".px.";
			left: 0;
			margin: 0 auto;
		}
		.infoblock_about { 
			width: $wb".px.";
			height: $x4".px.";
			position: absolute;
			top: 0;
			left: 0;
			font-size: $x3_5".px.";
			color: rgb(0,141,210);
			line-height: $x4".px.";
			text-align: center;
		}
		.infoblock_name_big {
			width: $x41_5".px.";
			height: $x9".px.";
			background: rgb(254,254,254);
			border-style: solid;
			border-radius: $x5_5".px.";
			border-width: $x1".px.";
			border-color: rgb(162,217,247);
			position: absolute;
			top: $x5_5".px.";
			left: $x1".px.";
			font-size: $x5".px.";
			color: rgb(0,141,210);
			line-height: $x9".px.";
			text-align: center;
		}
		.infoblock_phone_big {
			width: $x41_5".px.";
			height: $x9".px.";
			background: rgb(254,254,254);
			border-style: solid;
			border-radius: $x5_5".px.";
			border-width: $x1".px.";
			border-color: rgb(0,141,210);
			position: absolute;
			top: $x5_5".px.";
			left: $x44_5".px.";
			font-size: $x5".px.";
			color: rgb(0,141,210);
			line-height: $x9".px.";
			text-align: center;
		}
		.infoblock_send_big {
			width: $x11".px.";
			height: $x11".px.";
			background: rgb(0,141,210);
			border-style: solid;
			border-radius: $x5_5".px.";
			border-width: $x1".px.";
			border-color: rgb(162,217,247);
			position: absolute;
			top: $x5_5".px.";
			left: $x88".px.";
			font-size: $x5".px.";
			color: rgb(254,254,254);
			line-height: $x9".px.";
			text-align: center;
		}
		.infoblock_specific {
			width: $x97".px.";
			height: $x4".px.";
			background: rgb(254,254,254);
			border-style: solid;
			border-radius: $x2_5".px.";
			border-width: $x0_5".px.";
			border-color: rgb(162,217,247);
			position: absolute;
			top: $x5".px.";
			left: $x1".px.";
			font-size: $x3_5".px.";
			color: rgb(0,141,210);
			line-height: $x4".px.";
			text-align: center;
		}
		.infoblock_photo {
			width: $x31_5".px.";
			height: $x4".px.";
			background: rgb(254,254,254);
			border-style: solid;
			border-radius: $x2_5".px.";
			border-width: $x0_5".px.";
			border-color: rgb(162,217,247);
			position: absolute;
			top: $x12".px.";
			left: $x1".px.";
			font-size: $x3_5".px.";
			color: rgb(0,141,210);
			line-height: $x4".px.";
			text-align: center;
		}
		.infoblock_promo {
			width: $x31_5".px.";
			height: $x4".px.";
			background: rgb(254,254,254);
			border-style: solid;
			border-radius: $x2_5".px.";
			border-width: $x0_5".px.";
			border-color: rgb(162,217,247);
			position: absolute;
			top: $x12".px.";
			left: $x33_5".px.";
			font-size: $x3_5".px.";
			color: rgb(0,141,210);
			line-height: $x4".px.";
			text-align: center;
		}
		.infoblock_send_little {
			width: $x32".px.";
			height: $x5".px.";
			background: rgb(162,217,247);
			border-style: solid;
			border-radius: $x2_5".px.";
			border-width: $x0_5".px.";
			border-color: rgb(0,141,210);
			position: absolute;
			top: $x12".px.";
			left: $x66".px.";
			font-size: $x3_5".px.";
			color: rgb(0,141,210);
			line-height: $x4".px.";
			text-align: center;
		}
		.infoblock_name_little {
			width: $x31_5".px.";
			height: $x4".px.";
			background: rgb(254,254,254);
			border-style: solid;
			border-radius: $x2_5".px.";
			border-width: $x0_5".px.";
			border-color: rgb(162,217,247);
			position: absolute;
			top: $x12".px.";
			left: $x1".px.";
			font-size: $x3_5".px.";
			color: rgb(0,141,210);
			line-height: $x4".px.";
			text-align: center;
		}
		.infoblock_phone_little {
			width: $x31_5".px.";
			height: $x4".px.";
			background: rgb(254,254,254);
			border-style: solid;
			border-radius: $x2_5".px.";
			border-width: $x0_5".px.";
			border-color: rgb(0,141,210);
			position: absolute;
			top: $x12".px.";
			left: $x33_5".px.";
			font-size: $x3_5".px.";
			color: rgb(0,141,210);
			line-height: $x4".px.";
			text-align: center;
		}
		.infoblock_name_middle {
			width: $x48".px.";
			height: $x4".px.";
			background: rgb(254,254,254);
			border-style: solid;
			border-radius: $x2_5".px.";
			border-width: $x0_5".px.";
			border-color: rgb(162,217,247);
			position: absolute;
			top: $x5".px.";
			left: $x1".px.";
			font-size: $x3_5".px.";
			color: rgb(0,141,210);
			line-height: $x4".px.";
			text-align: center;
		}
		.infoblock_phone_middle {
			width: $x48".px.";
			height: $x4".px.";
			background: rgb(254,254,254);
			border-style: solid;
			border-radius: $x2_5".px.";
			border-width: $x0_5".px.";
			border-color: rgb(0,141,210);
			position: absolute;
			top: $x5".px.";
			left: $x50".px.";
			font-size: $x3_5".px.";
			color: rgb(0,141,210);
			line-height: $x4".px.";
			text-align: center;
		}
		.infoblock_send_middle {
			width: $x48".px.";
			height: $x5".px.";
			background: rgb(162,217,247);
			border-style: solid;
			border-radius: $x2_5".px.";
			border-width: $x0_5".px.";
			border-color: rgb(0,141,210);
			position: absolute;
			top: $x12".px.";
			left: $x25_5".px.";
			font-size: $x3_5".px.";
			color: rgb(0,141,210);
			line-height: $x4".px.";
			text-align: center;
		}
	</style>
	");
?>