<?php
	session_start();
	if (isset($_GET['name'])){
		$name = $_GET['name'];
		$_SESSION['name'] = $name;
	}else{
		$name = "";
	}
	if (isset($_GET['phone'])){
		$phone = $_GET['phone'];
		$_SESSION['phone'] = $phone;
	}else{
		$phone = "";
	}
	if (isset($_GET['specific'])){
		$specific = $_GET['specific'];
		$_SESSION['specific'] = $specific;
	}else{
		$specific = "";
	}
	if (isset($_GET['photo'])){
		$photo = $_GET['photo'];
		$_SESSION['photo'] = $photo;
	}else{
		$photo = "";
	}
	if (isset($_GET['promo'])){
		$promo = $_GET['promo'];
		$_SESSION['promo'] = $promo;
	}else{
		$promo = "";
	}
	if (isset($_GET['text'])){
		$text = $_GET['text'];
		$_SESSION['text'] = $text;
	}else{
		$text = "";
	}
	$arr = array($name, $phone, $specific, $photo, $promo, $text);
	exit(json_encode($arr));
?>